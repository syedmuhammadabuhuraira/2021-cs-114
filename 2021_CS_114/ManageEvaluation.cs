﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CRUD_Operations;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace _2021_CS_114
{
    public partial class ManageEvaluation : Form
    {
        public ManageEvaluation()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into Evaluation values ( @Name,@TotalMarks,@TotalWeightage)", con);

            try
            {
                cmd.Parameters.AddWithValue("@Name", guna2TextBox1.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            try
            {
                cmd.Parameters.AddWithValue("@TotalMarks", int.Parse(guna2TextBox2.Text));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            try
            {
                cmd.Parameters.AddWithValue("@TotalWeightage", int.Parse(guna2TextBox3.Text));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            bool check = false;
            try
            {
                check = true;
                cmd.ExecuteNonQuery();
            }
            catch (Exception c)
            {
                check = false;
                MessageBox.Show(c.Message);
            }
            if (check == true)
            {
                MessageBox.Show("Data Saved Successfully ");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select * from Evaluation", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            guna2DataGridView1.DataSource = dt;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("DELETE FROM Evaluation WHERE Id=@Id", con);
            try
            {
                cmd.Parameters.AddWithValue("@Id", int.Parse(guna2TextBox4.Text));
            }
            catch (Exception c)
            {
                MessageBox.Show(c.Message);
            }
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception c)
            {
                MessageBox.Show(c.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand command = new SqlCommand("select * from Evaluation", con);
            SqlDataAdapter data = new SqlDataAdapter(command);
            DataTable table = new DataTable();
            data.Fill(table);
            int marks = 0;
            int weight = 0;
            bool check = false;
            string name = "";
            for (int i = 0; i < table.Rows.Count; i++)
            {
                DataRow row = table.Rows[i];
                if (int.Parse(row["Id"].ToString()) == int.Parse(guna2TextBox4.Text))
                {
                    marks = int.Parse(row["TotalMarks"].ToString());
                    weight = int.Parse(row["TotalWeightage"].ToString());
                    name = row["Name"].ToString();
                    break;
                }
            }
            SqlCommand cmd = new SqlCommand("Update Evaluation set  Name = @Name,TotalMarks=@TotalMarks,TotalWeightage=@TotalWeightage where Id=@Id", con);
            cmd.Parameters.AddWithValue("@Id", int.Parse(guna2TextBox4.Text));
            if (guna2TextBox2.Text != "")
            {
                try
                {
                    cmd.Parameters.AddWithValue("@TotalMarks", int.Parse(guna2TextBox2.Text));
                }
                catch (Exception c)
                {
                    MessageBox.Show(c.Message);
                }
            }
            if (guna2TextBox2.Text == "")
            {
                cmd.Parameters.AddWithValue("@TotalMarks", marks);
            }
            if (guna2TextBox2.Text != "")
            {
                try
                {
                    cmd.Parameters.AddWithValue("@TotalWeightage", int.Parse(guna2TextBox3.Text));
                }
                catch (Exception c)
                {
                    MessageBox.Show(c.Message);
                }
            }
            if (guna2TextBox3.Text == "")
            {
                cmd.Parameters.AddWithValue("@TotalWeightage", weight);
            }
            if (guna2TextBox1.Text != "")
            {
                try
                {
                    cmd.Parameters.AddWithValue("@Name", guna2TextBox1.Text);
                }
                catch (Exception c)
                {
                    MessageBox.Show(c.Message);
                }
            }
            if (guna2TextBox1.Text == "")
            {
                cmd.Parameters.AddWithValue("@Name", name);
            }

            try
            {
                cmd.ExecuteNonQuery();
                check = true;
            }
            catch (Exception c)
            {
                MessageBox.Show(c.Message);
                check = false;
            }
        }
    }
}
