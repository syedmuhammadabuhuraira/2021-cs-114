﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Reflection.Metadata;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp.text.pdf;
using iTextSharp.text;
using CRUD_Operations;

namespace _2021_CS_114
{
    public partial class starterform : Form
    {
        public starterform()
        {
            InitializeComponent();
        }

        private void guna2GradientButton3_Click(object sender, EventArgs e)
        {

        }

        private void guna2GradientButton1_Click(object sender, EventArgs e)
        {

        }

        private void guna2CustomGradientPanel1_Paint(object sender, PaintEventArgs e)
        {
            WinAPI.AnimateWindow(this.Handle, 2000, WinAPI.CENTER);
        }

        private void guna2GradientButton1_Click_1(object sender, EventArgs e)
        {
            
            Student a = new Student();
            a.Show();
        }

        private void guna2GradientButton2_Click(object sender, EventArgs e)
        {
            Advisor b = new Advisor();
            b.Show();
        }

        private void guna2GradientButton4_Click(object sender, EventArgs e)
        {
            GroupInfo f = new GroupInfo();
            f.Show();

        }

        private void starterform_Load(object sender, EventArgs e)
        {
            WinAPI.AnimateWindow(this.Handle, 4000, WinAPI.CENTER);
        }

        private void guna2GradientButton5_Click(object sender, EventArgs e)
        {
            ManageProjects G = new ManageProjects();          
            G.Show();
            WinAPI.AnimateWindow(this.Handle, 4000, WinAPI.VER_Negative);
        }

        private void guna2GradientButton3_Click_1(object sender, EventArgs e)
        {
            ManageEvaluation w = new ManageEvaluation();
            w.Show();
        }

        private void guna2GradientButton6_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT [Registration No.] , p.[Project ID],p.[Project Title],\r\np.GroupId,Evaluation.Name , GroupEvaluation.ObtainedMarks FROM\r\nEvaluation JOIN GroupEvaluation ON Evaluation.Id =\r\nGroupEvaluation.EvaluationId JOIN(SELECT(SELECT RegistrationNo\r\nFrom Student WHere Student.Id = GroupStudent.StudentId) AS\r\n[Registration No.], GroupStudent.GroupId, (SELECT Project.Id\r\nFROM Project WHERE Exists(SELECT GroupProject.ProjectId FROM\r\nGroupProject WHERE GroupProject.GroupId = GroupStudent.GroupId\r\nAND Project.Id = GroupProject.ProjectId)) AS[Project ID],\r\n(SELECT Project.Title FROM Project WHERE Exists(SELECT\r\nGroupProject.ProjectId FROM GroupProject WHERE\r\nGroupProject.GroupId = GroupStudent.GroupId AND Project.Id =\r\nGroupProject.ProjectId)) AS[Project Title] FROM GroupStudent)\r\nAS P ON GroupEvaluation.GroupId = P.GroupId Order BY\r\nP.GroupId\r\n", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            guna2DataGridView1.DataSource = dt;
            iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(PageSize.A4, 25, 25, 30, 30);

            // Set the output file path
            string outputPath = "output1.pdf";

            // Create a FileStream object to write the PDF to the output file
            FileStream file = new FileStream(outputPath, FileMode.Create);

            // Create a PDF writer that writes to the FileStream
            PdfWriter writer = PdfWriter.GetInstance(pdfDoc, file);

            // Open the document
            pdfDoc.Open();

            // Create a table to hold the query result
            PdfPTable table = new PdfPTable(dt.Columns.Count);

            // Add the table header
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                table.AddCell(new Phrase(dt.Columns[i].ColumnName));
            }

            // Add the table data
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    table.AddCell(new Phrase(dt.Rows[i][j].ToString()));
                }
            }

            // Add the table to the document
            pdfDoc.Add(table);

            // Close the document
            pdfDoc.Close();

            // Close the FileStream
            file.Close();
        }
    }
}
