﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Windows.Forms;
using CRUD_Operations;
using System.Text.RegularExpressions;

namespace _2021_CS_114
{
    public partial class Student : Form
    {
        public Student()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Student_Load(object sender, EventArgs e)
        {

        }

        private void guna2GradientButton4_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            {
                var con = Configuration.getInstance().getConnection();
                if (textBox5.Text == "" || textBox1.Text == "" || textBox6.Text == "" || textBox4.Text == "" || textBox8.Text == "" || textBox10.Text == "")
                {
                    MessageBox.Show("Please enter some value in textBox: ");
                }
                else if (textBox5.Text != "")
                {
                    SqlCommand command1 = new SqlCommand("select * from Person", con);
                    SqlDataAdapter data1 = new SqlDataAdapter(command1);
                    DataTable table = new DataTable();
                    data1.Fill(table);
                    SqlCommand cmd = new SqlCommand("Insert into Person values (@FirstName, @LastName,@Contact,@Email,@DateOfBirth,@Gender)", con);
                    if (textBox1.Text != "")
                    {
                        cmd.Parameters.AddWithValue("@FirstName", textBox1.Text);
                    }
                    else
                    {
                        MessageBox.Show("Please Enter Correct First Name Thanks:");
                    }

                    if (textBox6.Text != "")
                    {
                        cmd.Parameters.AddWithValue("@LastName", textBox6.Text);
                    }
                    else
                    {
                        MessageBox.Show("Please Enter Correct LAst Name Thanks:");
                    }
                    if (textBox4.Text != "")
                    {
                        cmd.Parameters.AddWithValue("@Contact", textBox4.Text);
                    }
                    else
                    {
                        MessageBox.Show("Please Enter Correct Contact Number Thanks:");
                    }

                    if (textBox8.Text != "")
                    {
                        cmd.Parameters.AddWithValue("@Email", textBox8.Text);
                        
                    }
                    else
                    {
                        MessageBox.Show("Please Enter correct email");
                    }




                    bool chekcer = false;
                    if (textBox10.Text != "")
                    {
                        try
                        {

                            cmd.Parameters.AddWithValue("@Gender", int.Parse(textBox10.Text));
                            chekcer = true;
                        }
                        catch (Exception c)
                        {
                            MessageBox.Show("Please Enter Correct value of gender 1 or 2");
                            chekcer = false;
                        }

                    }
                    else
                    {
                        MessageBox.Show("Please Enter Correct Gender Value (1 or 2) Thanks:");
                    }

                    DateTime dateOfBirth = this.guna2DateTimePicker1.Value;
                    cmd.Parameters.AddWithValue("@DateOfBirth", dateOfBirth.ToString("dd/MM/yyyy"));
                    if (chekcer == true)
                    {
                        cmd.ExecuteNonQuery();
                    }
                    SqlCommand command = new SqlCommand("select * from Person", con);
                    SqlDataAdapter data = new SqlDataAdapter(command);
                    DataTable table1 = new DataTable();
                    data.Fill(table1);
                    int studentId = 0;
                    if (chekcer == true)
                    {

                        command.ExecuteNonQuery();
                        for (int i = 0; i < table1.Rows.Count; i++)
                        {
                            DataRow row = table1.Rows[i];
                            studentId = int.Parse(row["Id"].ToString());
                        }
                        var con1 = Configuration.getInstance().getConnection();
                        SqlCommand cmd1 = new SqlCommand("Insert into Student values (@Id,@RegistrationNo)", con1);
                        cmd1.Parameters.AddWithValue("@Id", studentId);
                        cmd1.Parameters.AddWithValue("@RegistrationNo", textBox5.Text);
                        cmd1.ExecuteNonQuery();
                        if (studentId != 0 && chekcer == true)
                        {
                            MessageBox.Show("Successfully saved");
                            textBox6.Text = "";
                            textBox1.Text = "";
                            textBox4.Text = "";
                            textBox10.Text = "";
                            textBox8.Text = "";
                        }
                    }


                }
                else
                {
                    MessageBox.Show("Please Enter Correct RegistrationNo:");
                }

            }
        }

        private void textBox10_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Person a JOIN Student b on a.id=b.id  ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void textBox5_Validating(object sender, CancelEventArgs e)
        {
            if (textBox5.Text != "")
            {
                Regex regex = new Regex(@"^\d+-+[A-Za-z]+-+\d+$");

                // Check if the textbox text matches the pattern
                if (!regex.IsMatch(textBox5.Text))
                {
                    e.Cancel = true; // Cancel the validation
                    MessageBox.Show("Please enter a valid Reg # OR Clear Section to Exit.", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand command = new SqlCommand("select * from Student", con);
            SqlDataAdapter data = new SqlDataAdapter(command);
            DataTable table = new DataTable();
            data.Fill(table);
            string tempreg = "";
            int studentId = 0;
            for (int i = 0; i < table.Rows.Count; i++)
            {
                DataRow row = table.Rows[i];
                tempreg = row["RegistrationNo"].ToString();
                tempreg = tempreg.Trim();
                if (tempreg == textBox5.Text)
                {
                    studentId = int.Parse(row["Id"].ToString());

                }
            }
            command.ExecuteNonQuery();
            SqlCommand command1 = new SqlCommand("select * from Person", con);
            SqlDataAdapter data1 = new SqlDataAdapter(command1);
            table = new DataTable();
            data1.Fill(table);
            string tempLastName = "";
            string tempFirstName = "";
            string tempContact = "";
            string tempEmail = "";
            string tempdob = "";
            int tempGender = 0;
            DateTime dateOfBirth = this.guna2DateTimePicker1.Value;
            for (int i = 0; i < table.Rows.Count; i++)
            {
                DataRow row = table.Rows[i];
                int tempid = int.Parse(row["Id"].ToString());
                if (studentId == tempid)
                {
                    MessageBox.Show("Hi");
                    tempFirstName = row["FirstName"].ToString();
                    tempLastName = row["LastName"].ToString();
                    tempContact = row["Contact"].ToString();
                    tempEmail = row["Email"].ToString();
                    tempGender = int.Parse(row["Gender"].ToString());
                    DateTime datevalue;

                    if (DateTime.TryParse(row["DateOfBirth"].ToString(), out datevalue))
                    {
                        dateOfBirth = datevalue;
                        MessageBox.Show("" + dateOfBirth);
                    }

                }
            }
            command1.ExecuteNonQuery();
            SqlCommand cmd = new SqlCommand("Update Person set FirstName=@FirstName,LastName=@LastName, Contact=@Contact,Email=@Email,Gender=@Gender where Id=@Id", con);
            cmd.Parameters.AddWithValue("@Id", studentId);
            if (textBox6.Text != "")
                cmd.Parameters.AddWithValue("@LastName", textBox6.Text);
            if (textBox6.Text == "")
            {
                cmd.Parameters.AddWithValue("@LastName", tempLastName);
            }
            if (textBox1.Text != "")
                cmd.Parameters.AddWithValue("@FirstName", textBox1.Text);
            if (textBox1.Text == "")
            {
                cmd.Parameters.AddWithValue("@FirstName", tempFirstName);
            }
            if (textBox8.Text != "")
                cmd.Parameters.AddWithValue("@Email", textBox8.Text);
            if (textBox8.Text == "")
            {
                cmd.Parameters.AddWithValue("@Email", tempEmail);
            }
            if (textBox4.Text != "")
                cmd.Parameters.AddWithValue("@Contact", textBox4.Text);
            if (textBox4.Text == "")
            {
                cmd.Parameters.AddWithValue("@Contact", tempContact);
            }
            if (textBox10.Text != "")
                cmd.Parameters.AddWithValue("@Gender", textBox10.Text);
            if (textBox10.Text == "")
            {
                cmd.Parameters.AddWithValue("@Gender", tempGender);
            }
            cmd.Parameters.AddWithValue("@DateOfBirth", dateOfBirth.ToString("dd/MM/yyyy"));
            cmd.ExecuteNonQuery();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("DELETE FROM Student WHERE RegistrationNo=@RegistrationNo", con);
            cmd.Parameters.AddWithValue("@RegistrationNo", textBox5.Text);
            cmd.ExecuteNonQuery();
        }
    }
}
