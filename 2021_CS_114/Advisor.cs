﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using CRUD_Operations;
namespace _2021_CS_114
{
    public partial class Advisor : Form
    {
        public Advisor()
        {
            InitializeComponent();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void button9_Click(object sender, EventArgs e)
        {

        }

        private void Advisor_Load(object sender, EventArgs e)
        {
            WinAPI.AnimateWindow(this.Handle, 2000, WinAPI.CENTER);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            bool check = false;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into Advisor values (@Id, @Designation,@Salary)", con);
            try
            {
                cmd.Parameters.AddWithValue("@Id", int.Parse(textBox1.Text));
                check = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Please Enter Correct value:");
                check = false;

            }
            try
            {
                check = true;
                cmd.Parameters.AddWithValue("@Salary", float.Parse(textBox2.Text));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                check = false;

            }
            if (guna2ComboBox1.Text != "")
            {
                string title = guna2ComboBox1.Text;
                MessageBox.Show(title);
                int Id = 0;
                SqlCommand command = new SqlCommand("select * from Lookup", con);
                SqlDataAdapter data = new SqlDataAdapter(command);
                DataTable table = new DataTable();
                data.Fill(table);
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    DataRow row = table.Rows[i];
                    string cate = row["Value"].ToString();
                    cate = cate.Trim();
                    if (cate == title)
                    {
                        Id = int.Parse(row["Id"].ToString());
                    }
                }
                MessageBox.Show("" + Id);
                cmd.Parameters.AddWithValue("@Designation", Id);
                command.ExecuteNonQuery();
                try
                {

                    cmd.ExecuteNonQuery();
                }
                catch (Exception c)
                {
                    MessageBox.Show("Please Enter Correct Value");
                }

            }
            else
            {
                MessageBox.Show("Please Enter designation: ");
            }
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            this.Close();
            starterform f = new starterform();
            f.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Advisor", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            bool check = false;
            if (textBox1.Text != "")
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("DELETE FROM Advisor WHERE Id=@Id", con);
                try
                {
                    cmd.Parameters.AddWithValue("@Id", int.Parse(textBox1.Text));
                    check = true;
                }
                catch (Exception c)
                {
                    MessageBox.Show("Please Enter Correct Id: ");
                }
                if (check == true)
                    cmd.ExecuteNonQuery();
                MessageBox.Show("Data Deleted SuccessFully");
            }
            else
            {
                MessageBox.Show("Please Enter  Id");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand command = new SqlCommand("select * from Advisor", con);
            SqlDataAdapter data = new SqlDataAdapter(command);
            DataTable table = new DataTable();
            data.Fill(table);
            int id = 0;
            float salary = 0f;
            int titleId = 0;
            for (int i = 0; i < table.Rows.Count; i++)
            {
                DataRow row = table.Rows[i];
                id = int.Parse(row["Id"].ToString());
                if (id == int.Parse(textBox1.Text))
                {
                    salary = float.Parse(row["Salary"].ToString());
                    titleId = int.Parse(row["Designation"].ToString());
                }
            }
            bool check = false;
            var con1 = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Update Advisor set  Designation = @Designation,Salary=@Salary where Id=@Id", con1);
            cmd.Parameters.AddWithValue("@Id", textBox1.Text);
            if (textBox2.Text != "")
            {
                try
                {
                    cmd.Parameters.AddWithValue("@Salary", textBox2.Text);
                    check = true;
                }
                catch (Exception c)
                {
                    MessageBox.Show("Please Enter Correct Salary: ");
                    check = false;
                }
            }
            if (textBox2.Text == "")
            {
                cmd.Parameters.AddWithValue("@Salary", salary);
            }
            if (guna2ComboBox1.Text != "")
            {
                int newId = IdFinder(guna2ComboBox1.Text);
                cmd.Parameters.AddWithValue("@Designation", newId);
            }

            if (guna2ComboBox1.Text == "")
            {
                cmd.Parameters.AddWithValue("@Designation", titleId);
            }
            try
            {

                cmd.ExecuteNonQuery();
            }
            catch (Exception c)
            {
                MessageBox.Show("Please Enter Correct Value");
            }

        }

        int IdFinder(string temp)
        {
            var con = Configuration.getInstance().getConnection();
            int Id = 0;
            SqlCommand command = new SqlCommand("select * from Lookup", con);
            SqlDataAdapter data = new SqlDataAdapter(command);
            DataTable table = new DataTable();
            data.Fill(table);
            for (int i = 0; i < table.Rows.Count; i++)
            {
                DataRow row = table.Rows[i];
                string cate = row["Value"].ToString();
                cate = cate.Trim();
                if (cate == temp)
                {
                    Id = int.Parse(row["Id"].ToString());
                }
            }
            return Id;
        }
    }
}
    

