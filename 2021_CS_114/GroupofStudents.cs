﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CRUD_Operations;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace _2021_CS_114
{
    public partial class GroupofStudents : Form
    {
        public GroupofStudents()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void guna2TextBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void GroupofStudents_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into GroupStudent values (@GroupId, @StudentId,@Status,@AssignmentDate)", con);
            if (guna2TextBox1.Text != "")
            {
                try
                {

                    cmd.Parameters.AddWithValue("@GroupId", int.Parse(guna2TextBox1.Text));

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            if (guna2TextBox4.Text != "")
            {
                try
                {
                    cmd.Parameters.AddWithValue("@StudentId", int.Parse(guna2TextBox4.Text));
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }
            if (guna2ComboBox1.Text != "")
            {
                int Id = 0;
                SqlCommand command3 = new SqlCommand("select * from Lookup", con);
                SqlDataAdapter data3 = new SqlDataAdapter(command3);
                DataTable table3 = new DataTable();
                data3.Fill(table3);
                for (int i = 0; i < table3.Rows.Count; i++)
                {
                    DataRow row = table3.Rows[i];
                    string cate = row["Value"].ToString();
                    cate = cate.Trim();
                    if (cate == guna2ComboBox1.Text)
                    {
                        Id = int.Parse(row["Id"].ToString());
                    }
                }
                try
                {

                    cmd.Parameters.AddWithValue("@Status", Id);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            DateTime createdon = this.guna2DateTimePicker1.Value;
            cmd.Parameters.AddWithValue("@AssignmentDate", createdon.ToString("yyyy/dd/MM"));
            bool ch = false;
            try
            {
                ch = true;
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ch = false;
                MessageBox.Show(ex.Message);
            }
            if (ch == true)
            {
                MessageBox.Show("Data Save SuccessFully : ");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("DELETE FROM GroupStudent WHERE GroupId=@GroupId  and StudentId=@StudentId", con);
            if (guna2TextBox4.Text != "")
            {
                try
                {
                    cmd.Parameters.AddWithValue("@GroupId", int.Parse(guna2TextBox4.Text));
                }
                catch (Exception c)
                {
                    MessageBox.Show(c.Message);
                }
            }
            else
            {
                MessageBox.Show("Please Enter Grupe Id");
            }
            if (guna2TextBox4.Text != "")
            {
                try
                {
                    cmd.Parameters.AddWithValue("@StudentId", int.Parse(guna2TextBox4.Text));
                }
                catch (Exception c)
                {
                    MessageBox.Show(c.Message);
                }
            }
            else
            {
                MessageBox.Show("Please Enter Correct Student Id");
            }
            bool check = false;
            try
            {
                cmd.ExecuteNonQuery();
                check = true;
            }
            catch (Exception c)
            {
                MessageBox.Show(c.Message);
            }
            if (check == true)
            {
                MessageBox.Show("Data deleted Successfully");
            }
        }

        private void guna2DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select GroupStudent.GroupId, Student.Id,GroupStudent.Status,GroupStudent.AssignmentDate from [GroupStudent] join Student on GroupStudent.StudentId = Student.Id", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int Group_id = 0;
            int Stuent_id = 0;
            int lookup_id = 0;
            int count = 0;
            var con = Configuration.getInstance().getConnection();
            // for taking Group id
            SqlCommand cmd1 = new SqlCommand("Select * from GroupStudent", con);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            if (dataGridView1.Rows.Count != 1)
            {
                int rowindex1 = dataGridView1.CurrentCell.RowIndex;
                var cellValue = dataGridView1.Rows[rowindex1].Cells[0].Value;
                Group_id = int.Parse(cellValue.ToString());
                // MessageBox.Show(project_id.ToString());
                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    DataRow row = dt1.Rows[i];
                    if (Group_id == int.Parse(row["GroupID"].ToString()))
                    {
                        count++;
                        break;
                    }

                }
            }
            else
            {
                MessageBox.Show("data grid view is empty");
            }
            // for taking student id.
            if (dataGridView1.Rows.Count != 1)
            {
                int rowindex1 = dataGridView1.CurrentCell.RowIndex;
                var cellValue = dataGridView1.Rows[rowindex1].Cells[0].Value;
                Stuent_id = int.Parse(cellValue.ToString());
                // MessageBox.Show(project_id.ToString());
                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    DataRow row = dt1.Rows[i];
                    if (Stuent_id == int.Parse(row["Studentid"].ToString()))
                    {
                        count++;
                        break;
                    }

                }
            }
            else
            {
                MessageBox.Show("data grid view is empty");
            }

            // taking look up id 

            //var con1 = Configuration.getInstance().getConnection();
            SqlCommand comd = new SqlCommand("Select * from Lookup", con);
            SqlDataAdapter da = new SqlDataAdapter(comd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow row = dt.Rows[i];
                //MessageBox.Show(comboBox2.Text);
                if (guna2ComboBox1.Text == row["Value"].ToString())
                {
                    lookup_id = int.Parse(row["id"].ToString());
                    break;
                }

            }
            if (count != 2)
            {
                DateTime d = this.guna2DateTimePicker1.Value;
                SqlCommand cmd = new SqlCommand("Insert into GroupStudent Values(@GroupId,@StudentId,@Status,@AssignmentDate) ", con);
                try
                {
                    cmd.Parameters.AddWithValue("@GroupId", Group_id);
                }
                catch (Exception c)
                {
                    MessageBox.Show(c.Message);
                }
                try
                {
                    cmd.Parameters.AddWithValue("@StudentId", Stuent_id);
                }
                catch (Exception c)
                {
                    MessageBox.Show(c.Message);
                }
                try
                {
                    cmd.Parameters.AddWithValue("@Status", lookup_id);
                }
                catch (Exception c)
                {
                    MessageBox.Show(c.Message);
                }
                try
                {
                    cmd.Parameters.AddWithValue("@AssignmentDate", d.ToString("dd/MM/yyyy"));
                }
                catch (Exception c)
                {
                    MessageBox.Show(c.Message);
                }
                bool checker = false;
                try
                {
                    checker = true;
                    cmd.ExecuteNonQuery();
                }
                catch (Exception c)
                {
                    MessageBox.Show(c.Message);
                    checker = false;

                }
                if (checker == true)
                {
                    MessageBox.Show("Data Saved Succcessfully");
                }
            }
            else
            {
                MessageBox.Show("There is a already used advisor or project ids :)" + count.ToString());
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {

        }
    }
}
