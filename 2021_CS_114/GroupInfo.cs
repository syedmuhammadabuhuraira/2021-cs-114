﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CRUD_Operations;

namespace _2021_CS_114
{
    public partial class GroupInfo : Form
    {
        public GroupInfo()
        {
            InitializeComponent();
        }

        private void guna2HtmlLabel1_Click(object sender, EventArgs e)
        {

        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            GroupofStudents t= new GroupofStudents();
            t.Show();

                }

        private void guna2DateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            
        }

        private void guna2Button2_Click(object sender, EventArgs e)
        {
           
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into GroupProject values (@ProjectId,@GroupId,@AssignmentDate)", con);
            //SqlCommand cmd1 = new SqlCommand("select * from [Group] ", con);
            //SqlCommand cmd2 = new SqlCommand("select * from [Project] ", con);
            //SqlDataAdapter data = new SqlDataAdapter(cmd1);
            //SqlDataAdapter data1 = new SqlDataAdapter(cmd2);
            //DataTable table = new DataTable();
            //DataTable table1 = new DataTable();
            //data.Fill(table);
            //data1.Fill(table1);
            //bool check1 = false, check = false;
            //for (int i = 0; i < table1.Rows.Count; i++)
            //{
            //    DataRow row = table1.Rows[i];
            //    if (int.Parse(projectIdBox.Text) == int.Parse(row["Id"].ToString()))
            //    {
            //        check1 = true;
            //        break;
            //    }
            //}
            //for (int i = 0; i < table.Rows.Count; i++)
            //{
            //    DataRow row = table.Rows[i];
            //    if (int.Parse(groupidBox.Text) == int.Parse(row["Id"].ToString()))
            //    {
            //        check = true;
            //        break;
            //    }
            //}

            try
            {
                cmd.Parameters.AddWithValue("@ProjectId", int.Parse(guna2TextBox2.Text));
            }
            catch (Exception c)
            {
                MessageBox.Show(c.Message);
            }
            try
            {
                cmd.Parameters.AddWithValue("@GroupId", int.Parse(guna2TextBox1.Text));
            }
            catch (Exception c)
            {
                MessageBox.Show(c.Message);
            }

            cmd.Parameters.AddWithValue("@AssignmentDate", dateTimePicker1.Value);
            try
            {
                cmd.ExecuteNonQuery();
            }

            catch (Exception c)
            {
                MessageBox.Show(c.Message);
            }
        }

        private void guna2Button3_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select GroupProject.ProjectId,GroupProject.GroupId,Project.Title,GroupProject.AssignmentDate from GroupProject join Project on Project.Id = GroupProject.ProjectId", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            guna2DataGridView1.DataSource = dt;
        }

        private void guna2DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void guna2Button4_Click(object sender, EventArgs e)
        {
            bool check = false;

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("DELETE FROM GroupProject WHERE ProjectId=@ProjectId and GroupId=@GroupId", con);
            if (guna2TextBox1.Text != "")
            {
                try
                {
                    cmd.Parameters.AddWithValue("@GroupId", int.Parse(guna2TextBox1.Text));
                }
                catch (Exception c)
                {
                    MessageBox.Show(c.Message);
                }
            }
            else
            {
                MessageBox.Show("Please Enter Group Id");
            }
            if (guna2TextBox2.Text != "")
            {
                try
                {
                    cmd.Parameters.AddWithValue("@projectId", int.Parse(guna2TextBox2.Text));
                }
                catch (Exception c)
                {
                    MessageBox.Show(c.Message);
                }
            }
            else
            {
                MessageBox.Show("Please Enter Project Id");
            }
            try
            {
                check = true;
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
                check = false;
                MessageBox.Show("Please Wrtite COrrect Value");
            }
            if (check == true)
                MessageBox.Show("Data Deleted SuccessFully");

        }

        private void guna2Button5_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select * from GroupProject", con);
            SqlDataAdapter data = new SqlDataAdapter(cmd);
            DataTable table = new DataTable();
            data.Fill(table);
            int groupId = 0;
            int ProjectId = 0;
            for (int i = 0; i < table.Rows.Count; i++)
            {
                DataRow row = table.Rows[i];

            }
        }
        //private void addProjectIdInBox()
        //{
        //    var con = Configuration.getInstance().getConnection();
        //    SqlCommand cmd = new SqlCommand("SELECT * FROM [Project]", con);

        //    SqlDataAdapter da = new SqlDataAdapter(cmd);
        //    DataTable dt = new DataTable();
        //    da.Fill(dt);
        //    for (int i = 0; i < dt.Rows.Count; i++)
        //    {
        //        DataRow row = dt.Rows[i];
        //        guna2ComboBox1.Items.Add(row["Id"].ToString());
        //    }

        //}
        //private void addGroupIdInBox()
        //{
        //    var con = Configuration.getInstance().getConnection();
        //    SqlCommand cmd = new SqlCommand("SELECT * FROM [Group]", con);

        //    SqlDataAdapter da = new SqlDataAdapter(cmd);
        //    DataTable dt = new DataTable();
        //    da.Fill(dt);
        //    for (int i = 0; i < dt.Rows.Count; i++)
        //    {
        //        DataRow row = dt.Rows[i];
        //        guna2ComboBox2.Items.Add(row["Id"].ToString());
        //    }

        //}

        private void guna2ComboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
