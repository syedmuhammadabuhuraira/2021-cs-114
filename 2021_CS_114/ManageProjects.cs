﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRUD_Operations;
using System.Windows.Forms;

namespace _2021_CS_114
{
    public partial class ManageProjects : Form
    {
        public ManageProjects()
        {
            InitializeComponent();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Project", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            guna2DataGridView1.DataSource = dt;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("DELETE FROM Project WHERE Id=@Id", con);
            if (textBox4.Text != "")
                cmd.Parameters.AddWithValue("@Id", textBox4.Text);
            else
            {
                MessageBox.Show("Please Enter Id");
            }
            try
            {

                cmd.ExecuteNonQuery();
            }
            catch (Exception c)
            {
                MessageBox.Show("Please Wrtite COrrect Value");
            }
            MessageBox.Show("Data Deleted SuccessFully");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into Project values (@Description, @Title)", con);
            try
            {
                cmd.Parameters.AddWithValue("@Title", textBox2.Text);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Please Enter Title:");
            }
            try
            {
                if (textBox3.Text != "")
                    cmd.Parameters.AddWithValue("@Description", textBox3.Text);
                else
                {
                    MessageBox.Show("Please Enter Value");
                }

            }
            catch (Exception c)
            {
                MessageBox.Show("please Enter Correct Value");
            }
            try
            {
                cmd.ExecuteNonQuery();

            }
            catch (Exception c)
            {
                MessageBox.Show("Please Enter Correct Value: ");
            }
            MessageBox.Show("Successfully saved");
            textBox4.Enabled = true;
        }

        private void show_button_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Project", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            guna2DataGridView1.DataSource = dt;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand command = new SqlCommand("select * from Project", con);
            SqlDataAdapter data = new SqlDataAdapter(command);
            DataTable table = new DataTable();
            data.Fill(table);
            string ttile = "";
            string description = "";
            int id = 0;
            for (int i = 0; i < table.Rows.Count; i++)
            {
                DataRow row = table.Rows[i];
                id = int.Parse(row["Id"].ToString());
                if (id == int.Parse(textBox4.Text))
                {
                    ttile = row["Title"].ToString();
                    description = row["Description"].ToString();
                    break;
                }

            }
            bool check = false;
            var con1 = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Update Project set  Description = @Description,Title=@Title where Id=@Id", con1);
            cmd.Parameters.AddWithValue("@Id", id);
            if (textBox2.Text != "")
            {
                try
                {
                    cmd.Parameters.AddWithValue("@Title", textBox2.Text);
                    check = true;
                }
                catch (Exception c)
                {
                    MessageBox.Show("Please Enter Correct Title: ");
                    check = false;
                }
            }
            if (textBox2.Text == "")
            {
                cmd.Parameters.AddWithValue("@Title", ttile);
            }
            if (textBox3.Text != "")
            {
                try
                {
                    cmd.Parameters.AddWithValue("@Description", textBox3.Text);
                    check = true;
                }
                catch (Exception c)
                {
                    MessageBox.Show("Please Enter Correct Description: ");
                    check = false;
                }
            }
            if (textBox3.Text == "")
            {
                cmd.Parameters.AddWithValue("@Description", description);
            }
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception c)
            {
                MessageBox.Show("Please Enter Value");
            }

        }

        private void ManageProjects_Load(object sender, EventArgs e)
        {
            WinAPI.AnimateWindow(this.Handle, 2000, WinAPI.VER_Negative);
        }
    }
}
    

