﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CRUD_Operations;

namespace _2021_CS_114
{
    public partial class ProjectAdvisor : Form
    {
        public ProjectAdvisor()
        {
            InitializeComponent();
        }

        private bool isIdPresent(int advisorId, string idname)
        {
            idname = idname + "Id";
            var con = Configuration.getInstance().getConnection();
            SqlCommand command = new SqlCommand("select * from ProjectAdvisor", con);
            SqlDataAdapter data = new SqlDataAdapter(command);
            DataTable table = new DataTable();
            data.Fill(table);
            for (int i = 0; i < table.Rows.Count; i++)
            {
                DataRow row = table.Rows[i];
                if (advisorId == int.Parse(row[idname].ToString()))
                {
                    return true;
                }
            }
            return false;
        }
        private void button3_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand command = new SqlCommand("select * from Project", con);
            SqlCommand command1 = new SqlCommand("select * from Advisor", con);
            SqlCommand command2 = new SqlCommand("select * from ProjectAdvisor", con);
            SqlDataAdapter data = new SqlDataAdapter(command);
            SqlDataAdapter data1 = new SqlDataAdapter(command1);
            SqlDataAdapter data2 = new SqlDataAdapter(command2);
            DataTable table = new DataTable();
            DataTable table1 = new DataTable();
            DataTable table2 = new DataTable();
            data.Fill(table);
            data1.Fill(table1);
            data2.Fill(table2);
            int projectCount = table.Rows.Count;
            int advisorCount = table1.Rows.Count;
            int projectAdvisorcount = table2.Rows.Count;
            MessageBox.Show(projectCount + "" + projectAdvisorcount);
            if (projectCount > projectAdvisorcount && projectCount >= advisorCount)
            {
                SqlCommand cmd = new SqlCommand("Insert into ProjectAdvisor values (@AdvisorId,@ProjectId, @AdvisorRole,@AssignmentDate)", con);
                int advisorId = 0;
                for (int i = 0; i < table1.Rows.Count; i++)
                {
                    DataRow row = table1.Rows[i];
                    advisorId = int.Parse(row["Id"].ToString());
                    if (isIdPresent(advisorId, "Advisor") == false)
                    {
                        break;
                    }
                }
                int projectId = 0;
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    DataRow row = table.Rows[i];
                    projectId = int.Parse(row["Id"].ToString());

                    if (isIdPresent(projectId, "Project") == false)
                    {
                        break;
                    }
                }
                cmd.Parameters.AddWithValue("@ProjectId", projectId);
                cmd.Parameters.AddWithValue("@AdvisorId", advisorId);
                MessageBox.Show(guna2TextBox3.Text + " role ");
                if (guna2TextBox3.Text != "")
                {
                    int Id = 0;
                    SqlCommand command3 = new SqlCommand("select * from Lookup", con);
                    SqlDataAdapter data3 = new SqlDataAdapter(command3);
                    DataTable table3 = new DataTable();
                    data3.Fill(table3);
                    for (int i = 0; i < table3.Rows.Count; i++)
                    {
                        DataRow row = table3.Rows[i];
                        string cate = row["Value"].ToString();
                        cate = cate.Trim();
                        if (cate == guna2TextBox3.Text)
                        {
                            Id = int.Parse(row["Id"].ToString());
                        }
                    }
                    cmd.Parameters.AddWithValue("@AdvisorRole", Id);
                    DateTime dateOfBirth = this.dateTimePicker1.Value;
                    cmd.Parameters.AddWithValue("@AssignmentDate", dateOfBirth.ToString("dd/MM/yyyy"));
                    command.ExecuteNonQuery();
                }
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception c)
                {
                    MessageBox.Show(c.Message);
                }
            }
            else
            {
                MessageBox.Show("You are unable to add project please add project first then you will be able to add project advisor");
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from ProjectAdvisor", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("DELETE FROM ProjectAdvisor WHERE ProjectId=@ProjectId", con);
            if (guna2TextBox2.Text != "")
            {
                try
                {
                    cmd.Parameters.AddWithValue("@ProjectId", int.Parse(guna2TextBox2.Text));

                }
                catch (Exception c)
                {
                    MessageBox.Show(c.Message);
                }
            }
            else
            {
                MessageBox.Show("Please Enter Id");
            }
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
                MessageBox.Show("Please Wrtite Correct Value");
            }
            MessageBox.Show("Data Deleted Successfully ");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand command = new SqlCommand("select * from ProjectAdvisor", con);
            SqlDataAdapter data = new SqlDataAdapter(command);
            DataTable table = new DataTable();
            data.Fill(table);
            int role = 0;
            int id = 0;
            int advisorid = 0;
            DateTime assignmentdate = this.dateTimePicker1.Value;
            DateTime newAssignmentdate = this.dateTimePicker1.Value;

            for (int i = 0; i < table.Rows.Count; i++)
            {
                DataRow row = table.Rows[i];
                id = int.Parse(row["ProjectId"].ToString());
                if (id == int.Parse(guna2TextBox2.Text))
                {
                    role = int.Parse(row["AdvisorRole"].ToString());
                    advisorid = int.Parse(row["AdvisorId"].ToString());
                    DateTime datevalue;
                    if (DateTime.TryParse(row["AssignmentDate"].ToString(), out datevalue))
                    {
                        newAssignmentdate = datevalue;
                    }
                    break;
                }

            }
            if (newAssignmentdate != assignmentdate)
            {
                newAssignmentdate = assignmentdate;
            }
            bool check = false;
            var con1 = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Update ProjectAdvisor set  AdvisorId = @AdvisorId,AdvisorRole=@AdvisorRole,AssignmentDate=@AssignmentDate where ProjectId=@ProjectId", con1);
            cmd.Parameters.AddWithValue("@ProjectId", id);
            if (guna2TextBox1.Text != "")
            {
                try
                {
                    bool ch = advisorIdFinder(int.Parse(guna2TextBox1.Text));
                    if (ch == true)
                        cmd.Parameters.AddWithValue("@AdvisorId", int.Parse(guna2TextBox1.Text));
                    else
                    {
                        MessageBox.Show("There is no such advisor Id: " +
                            "Please Enter Correct Advisor Id");
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Please Enter Correct AdvisorID: ");
                }
                if (guna2TextBox1.Text == "")
                {
                    cmd.Parameters.AddWithValue("@AdvisorId", advisorid);
                }
                if (guna2TextBox3.Text != "")
                {
                    try
                    {
                        int roleID1 = IdFinder(guna2TextBox3.Text);
                        cmd.Parameters.AddWithValue("@AdvisorRole", roleID1);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Please Enter Correct AdvisorRole: ");
                    }
                }
                if (guna2TextBox3.Text == "")
                {
                    cmd.Parameters.AddWithValue("@AdvisorRole", role);
                }
                cmd.Parameters.AddWithValue("@AssignmentDate", newAssignmentdate.ToString("dd/MM/yyyy"));
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception)
                {
                    MessageBox.Show("Please Enter Correct Value");
                }
            }
            bool advisorIdFinder(int id)
            {
                var con = Configuration.getInstance().getConnection();
                int Id = 0;
                SqlCommand command = new SqlCommand("select * from Advisor", con);
                SqlDataAdapter data = new SqlDataAdapter(command);
                DataTable table = new DataTable();
                data.Fill(table);
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    DataRow row = table.Rows[i];
                    if (id == int.Parse(row["Id"].ToString()))
                    {
                        return true;
                    }

                }
                return false;

            }
            int IdFinder(string temp)
            {
                var con = Configuration.getInstance().getConnection();
                int Id = 0;
                SqlCommand command = new SqlCommand("select * from Lookup", con);
                SqlDataAdapter data = new SqlDataAdapter(command);
                DataTable table = new DataTable();
                data.Fill(table);
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    DataRow row = table.Rows[i];
                    string cate = row["Value"].ToString();
                    cate = cate.Trim();
                    if (cate == temp)
                    {
                        Id = int.Parse(row["Id"].ToString());
                    }
                }
                return Id;
            }


        }
    }
}
